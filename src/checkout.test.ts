// Import the modules
import chai from 'chai';
import Checkout from './checkout';
const pricingRules = require('./pricingRules.json');

const expect = chai.expect;

describe('checkout system', function () {
    it('should throw error as item not exist', function () {
        try {
            const co = new Checkout(pricingRules);
            co.scan('notExist');
            expect.fail('No error was thrown.');
        } catch (error) {
            expect(error).to.be.an.instanceOf(Error);
            expect(error.message).to.equal('Item not exist');
        }
    });

    it('should return total 1109.97 - buy 3 for price of 2', function () {
        const co = new Checkout(pricingRules);
        co.scan('buds');
        co.scan('op10');
        co.scan('buds');
        co.scan('buds');

        /** get total */
        let result = co.getTotal();
        expect(result).to.equal(1109.97);
    });

    it('should return total 389.97 - buy 3 for price of 2', function () {
        const co = new Checkout(pricingRules);
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');

        /** get total */
        let result = co.getTotal();
        expect(result).to.equal(389.97);
    });

    it('should return total 519.96 - buy 3 for price of 2 - for 5 buds', function () {
        const co = new Checkout(pricingRules);
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');

        /** get total */
        let result = co.getTotal();
        expect(result).to.equal(519.96);
    });

    it('should return total 519.96 - buy 3 for price of 2 - for 6 buds', function () {
        const co = new Checkout(pricingRules);
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');
        co.scan('buds');

        /** get total */
        let result = co.getTotal();
        expect(result).to.equal(519.96);
    });

    it('should return total - bulk offer', function () {
        const co = new Checkout(pricingRules);
        co.scan('wtch');
        co.scan('op11');
        co.scan('op11');
        co.scan('op11');
        co.scan('buds');
        co.scan('buds');
        co.scan('op11');
        co.scan('op11');

        /** get total */
        let result = co.getTotal();
        expect(result).to.equal(4989.92);
    });

    it('should return a zero', function () {
        const co = new Checkout(pricingRules);
        const result = co.getTotal();
        expect(result).to.equal(0);
        expect(result).to.be.a('number');
    });

    it('should return object of cart item', function () {
        const co = new Checkout(pricingRules);
        co.scan('op11');
        const result = co.getCart();
        expect(result['op11']).to.exist;
    });
});
