import readline from 'readline';
import Checkout from './checkout';
const pricingRules = require('./pricingRules.json');

/** Create an interface for reading input */
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

/** Function to display the menu options */
function displayMenu() {
    console.log('Menu Options:');
    console.log('1. Scan item');
    console.log('2. Get total');
    console.log('3. Exit');
}

const co = new Checkout(pricingRules);

/** Function to perform the selected action */
function performAction(option) {
    switch (option) {
        case '1':
            clearConsole();
            handleScanItem();
            break;
        case '2':
            clearConsole();
            console.log('Total is: ', co.getTotal());
            promptUser();
            break;
        case '3':
            console.log('Exiting...');
            rl.close();
            process.exit(0);
        default:
            clearConsole();
            console.log('Invalid option');
            promptUser();
            break;
    }
}

function handleScanItem() {
    rl.question('Enter item name: ', (item) => {
        if (co.isItemExist(item)) {
            clearConsole();
            co.scan(item);
            console.log('Item scanned successfully');
            promptUser();
        } else {
            console.log(`${item} - not exist`);
            handleScanItem();
        }
    });
}

/** Function to prompt the user for input */
function promptUser() {
    displayMenu();
    rl.question('Select an option: ', (option) => {
        performAction(option);
    });
}

function clearConsole() {
    console.log('\u001Bc');
}

(() => {
    /** Start the prompt */
    promptUser();
})();
