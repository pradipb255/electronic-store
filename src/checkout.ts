const items = {
    op10: {
        name: 'Oneplus 10',
        price: 849.99
    },
    op11: {
        name: 'Oneplus 11',
        price: 949.99
    },
    buds: {
        name: 'Earbuds',
        price: 129.99
    },
    wtch: {
        name: 'Smart Watch',
        price: 229.99
    }
};

export default class Checkout {
    private pricingRules: {
        [sku: string]: {
            code: string;
            description: string;
            buyQuantity?: number;
            payQuantity?: number;
            price?: number;
            minQuantity?: number;
        };
    }[] = [];

    private cart: {
        [sku: string]: {
            quantity: number;
            total: number;
        };
    } = {};

    constructor(pricingRules) {
        this.pricingRules = pricingRules;
    }

    public scan(sku) {
        if (!this.isItemExist(sku)) {
            throw new Error('Item not exist');
        }
        if (this.cart[sku]) {
            this.cart[sku].quantity += 1;
            this.cart[sku].total = +Number(this.cart[sku].quantity * items[sku].price).toFixed(2);
        } else {
            this.cart[sku] = {
                quantity: 1,
                total: +Number(items[sku].price).toFixed(2)
            };
        }
    }

    public getCart() {
        return this.cart;
    }

    public getTotal() {
        let cartTotal = 0;
        for (const itemSKU in this.cart) {
            /** find offer for particular sku */
            let itemOffer = this.pricingRules[itemSKU];

            /** process offer if exist */
            if (itemOffer) {
                /** switch for every type of offer */
                switch (itemOffer.code) {
                    case 'BUY_3_PAY_FOR_2':
                        if (this.cart[itemSKU].quantity >= itemOffer.buyQuantity) {
                            this.cart[itemSKU].total =
                                Math.floor(this.cart[itemSKU].quantity / itemOffer.buyQuantity) *
                                    itemOffer.payQuantity *
                                    items[itemSKU].price +
                                (this.cart[itemSKU].quantity % itemOffer.buyQuantity) * items[itemSKU].price;
                        }
                        break;
                    case 'BULK_DISCOUNT':
                        if (this.cart[itemSKU].quantity > itemOffer.minQuantity) {
                            this.cart[itemSKU].total = this.cart[itemSKU].quantity * itemOffer.price;
                        }
                        break;
                    default:
                        break;
                }

                /** set decimal point */
                this.cart[itemSKU].total = +Number(this.cart[itemSKU].total).toFixed(2);
            }
            cartTotal += this.cart[itemSKU].total;
        }
        return cartTotal;
    }

    public isItemExist(item) {
        return items[item] ? true : false;
    }
}
